@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        {{-- <div class="box-table">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Points</th>
                    <th>Played</th>
                </tr>
                @forelse($histories as $history)
                <tr>
                    <td>{{ $history->user->name }}</td>
                    <td class="text-center">{{ $history->point > 0 ? '+'.$history->point : $history->point }}</td>
                    <td class="text-center">{{ $history->created_at->diffForHumans() }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="3">no data available</td>
                </tr>
                @endforelse
            </table>
            {{ $histories->links() }}
        </div> --}}
        <table class="table table-striped">
            <thead style="background: #dddd">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Points</th>
                    <th>Last Played</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse($histories as $history)
                <tr>
                    <td>{{ $history->name }}</td>
                    <td>{{ $history->email }}</td>
                    <td >{{ $history->point}}</td>
                    <td >{{ \Carbon\Carbon::parse($history->last_played)->diffForHumans() }}</td>
                    <td ><i class="far fa-eye" onclick="detail({{$history->id}})" style="font-size: 25px;" data-toggle="tooltip" data-placement="right" title="Detail"></i></td>
                </tr>
                @empty
                <tr>
                    <td colspan="3">no data available</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        {{ $histories->links() }}
    </div>
    {{-- MODAL --}}
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Play History</h5>
            </div>
            <div class="modal-body">
                <table id="table" class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Word</th>
                            <th scope="col">Answer Result</th>
                            <th scope="col">Accumulated Points</th>
                            <th scope="col">Play Time</th>
                          </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        function detail(user_id){
            $.ajax({
                method: 'GET',
                url: `get-detail/${user_id}`
            }).done(function(result) {
                if(result['code']==200){
                    $('#table > tbody').empty()
                    if(result['data'].length > 0){
                        var point = 0;
                        for (let index = 0; index < result['data'].length; index++) {
                            var no = index+1;
                            point = point + parseInt(result['data'][index]['point'])
                            table = "<tr>"+
                                        " <th scope='row'>"+no+"</th>"+
                                        " <td>"+result['data'][index]['word']+"</td>"+
                                        " <td>"+result['data'][index]['answer']+"</td>"+
                                        " <td>"+point+"</td>"+
                                        " <td>"+result['data'][index]['play_time']+"</td>"+
                                    " </tr>"
                            $('#table > tbody').append(table);
                        }
                        $('.modal').modal('show');
                    }else{
                        alert('Oopss no data play histori')
                    }
                }
            });
        }
    </script>
@endsection