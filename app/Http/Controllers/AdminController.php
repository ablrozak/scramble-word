<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\History;
use DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if (auth()->user()->is_admin == false) {
            auth()->logout();

            return redirect('/');
        }
        $sub = User::where('users.is_admin',0)->select(DB::raw("users.id,users.name,users.email,users.`point`,
                            (SELECT created_at FROM histories where user_id =users.id order by created_at desc limit 1) last_played"));
        $histories = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                            ->mergeBindings($sub->getQuery())
                            ->whereNotNull('sub.last_played')
                            ->paginate(25);
        return view('admin')->with('histories', $histories);
    }

    public function getDetail($user_id)
    {
        try {
            if (auth()->user()->is_admin == false) {
                auth()->logout();
    
                return redirect('/');
            }
            $histories = DB::select("SELECT a.user_id,w.word,case when a.`point` < 0 then 'Wrong' else 'Correct' end as answer,a.`point`,a.created_at
                                    FROM histories a 
                                    LEFT JOIN words w on a.word_id =w.id 
                                    WHERE a.user_id =3
                                    ORDER BY a.created_at asc");
            foreach($histories as $key => $value){
                $data[$key]['user_id'] = $value->user_id;
                $data[$key]['word'] = $value->word;
                $data[$key]['answer'] = $value->answer;
                $data[$key]['point'] = $value->point;
                $data[$key]['play_time'] = \Carbon\Carbon::parse($value->created_at)->diffForHumans();
            }
            $response = [
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $data
            ];
        } catch (\Exception $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            $response = [
                'code'      => 400,
                'message'   => 'Internal Server Error'
            ];
        }
        return response()->json($response);
    }
}
