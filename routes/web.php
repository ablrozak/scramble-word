<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('get-word', [App\Http\Controllers\GameController::class, 'getWord']);
Route::post('check-answer', [App\Http\Controllers\GameController::class, 'checkAnswer']);
Route::get('admin', [App\Http\Controllers\AdminController::class, 'index']);
Route::get('get-detail/{user_id}', [App\Http\Controllers\AdminController::class, 'getDetail']);

Auth::routes();
